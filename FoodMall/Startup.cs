﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FoodMall.Startup))]
namespace FoodMall
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
